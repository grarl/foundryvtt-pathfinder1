import { ActorPF } from "./actor-pf.mjs";
import { applyChanges } from "./utils/apply-changes.mjs";

export class ActorHauntPF extends ActorPF {
  prepareBaseData() {
    this._resetInherentTotals();

    this.changeFlags = {};
    /** @type {Record<string, SourceInfo>} */
    this.sourceInfo = {};

    // Needed for getRollData and ActorPF, but useless for the actor
    this.system.abilities = {
      str: {
        value: 10,
        damage: 0,
        drain: 0,
        userPenalty: 0,
        mod: 0,
        total: 10,
      },
      dex: {
        value: 10,
        damage: 0,
        drain: 0,
        userPenalty: 0,
        mod: 0,
        total: 10,
      },
      con: {
        value: 10,
        damage: 0,
        drain: 0,
        userPenalty: 0,
        mod: 0,
        total: 10,
      },
      int: {
        value: 10,
        damage: 0,
        drain: 0,
        userPenalty: 0,
        mod: 0,
        total: 10,
      },
      wis: {
        value: 10,
        damage: 0,
        drain: 0,
        userPenalty: 0,
        mod: 0,
        total: 10,
      },
      cha: {
        value: 10,
        damage: 0,
        drain: 0,
        userPenalty: 0,
        mod: 0,
        total: 10,
      },
    };

    this.system.attributes.attack ??= { general: 0, shared: 0 };

    this.system.skills ??= {};

    // Init resources structure
    this.system.resources ??= {};

    // Handle Health
    this.system.attributes.hp.max = this.system.attributes.hp.base;
  }

  _getInherentTotalsKeys() {
    return {
      "details.carryCapacity.bonus.total": 0,
      "details.carryCapacity.multiplier.total": 0,
    };
  }

  /**
   * Needed to prevent unnecessary behavior in ActorPF
   *
   * @override
   */
  prepareDerivedData() {
    this.system.details.cr.total = this.system.details.cr.base;
    this.system.attributes.init.total = this.system.attributes.init.value;

    for (const item of this.items) {
      item._prepareDependentData(false);
      this.updateItemResources(item);
    }

    applyChanges(this);

    this._prepareTemplates();
    this._prepareCR();
    this.prepareHealth();

    // Setup links
    this.prepareItemLinks();

    // Reset roll data cache again to include processed info
    this._rollData = null;

    // Update item resources
    for (const item of this.items) {
      item._prepareDependentData(true);
      // because the resources were already set up above, this is just updating from current roll data - so do not warn on duplicates
      this.updateItemResources(item, { warnOnDuplicate: false });
    }
  }

  prepareHealth() {
    // Offset relative health
    const hp = this.system.attributes.hp;
    if (!Number.isFinite(hp?.offset)) hp.offset = 0;
    hp.value = hp.max + hp.offset;
  }

  /**
   * Needed to prevent unnecessary behavior in ActorPF
   *
   * @override
   */
  refreshDerivedData() {}

  _prepareTypeChanges(changes) {
    // BAB
    changes.push(
      new pf1.components.ItemChange({
        _id: "_bab", // HACK: Force ID to be special
        formula: "@attributes.bab.total",
        operator: "add",
        target: "~attackCore",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.BAB"),
      })
    );
  }

  getRollData(options = { refresh: false, cache: true }) {
    // Return cached data, if applicable
    const skipRefresh = !options.refresh && this._rollData && options.cache;

    const result = { ...(skipRefresh ? this._rollData : pf1.utils.deepClone(this.system)) };

    // Clear certain fields if not refreshing
    if (skipRefresh) {
      for (const key of pf1.config.temporaryRollDataFields.actor) {
        delete result[key];
      }
    }

    /* ----------------------------- */
    /* Always add the following data
    /* ----------------------------- */

    // Add combat round, if in combat
    if (game.combats?.viewed) {
      result.combat = {
        round: game.combat.round || 0,
      };
    }

    // Caster Level
    result.cl = result.details.cl;

    // Aura strength
    result.auraStrength = this.auraStrength;

    // Return cached data, if applicable
    if (skipRefresh) return result;

    /* ----------------------------- */
    /* Set the following data on a refresh
    /* ----------------------------- */

    // Spoof size as Medium instead of letting it fail to Fine
    result.size = 4;

    // Add item dictionary flags
    result.dFlags = this.itemFlags?.dictionary ?? {};

    // Add range info
    result.range = pf1.documents.actor.ActorPF.getReach(this.system.traits.size, this.system.traits.stature);

    // Wound Threshold isn't applicable
    result.attributes.woundThresholds = { level: 0 };

    // Haunts don't have ACP
    result.attributes.acp = { attackPenalty: 0 };

    // Call hook
    if (Hooks.events["pf1GetRollData"]?.length > 0) Hooks.callAll("pf1GetRollData", this, result);

    this._rollData = result;
    return result;
  }

  _prepareCR() {
    // Gather CR Offset
    let crOffset = 0;
    for (const key of this.system.templates.base) {
      const crDetail = pf1.config.hauntTemplates[key];
      if (!crDetail) continue;
      crOffset += crDetail.cr;

      // Check for bonus CR - this is a special case for when a template's
      // CR bonus is increased by the existence of a template with a specific tag.
      // In 1PP, this is only used for the "Elusive" template when "Persistent" is also applied.
      const crBonusTag = crDetail.crBonusTag;
      if (crBonusTag && this.system.templates.base.includes(crBonusTag)) {
        crOffset += crDetail.crBonus;
      }
    }

    // Reset CR
    this.system.details.cr.total = this.system.details.cr.base + crOffset;

    // Reset experience value
    let newXP = 0;
    try {
      const crTotal = this.system.details?.cr?.total || 0;
      newXP = pf1.utils.CR.getXP(crTotal);
    } catch {
      newXP = pf1.utils.CR.getXP(1);
    }
    foundry.utils.setProperty(this.system, "details.xp.value", newXP);
  }

  /**
   * Aura strength as number from 0 to 4.
   *
   * @see {@link pf1.config.auraStrengths}
   *
   * @type {number}
   */
  get auraStrength() {
    const cl = this.system.details.cl || 0;
    if (cl < 1) {
      return 0;
    } else if (cl < 6) {
      return 1;
    } else if (cl < 12) {
      return 2;
    } else if (cl < 21) {
      return 3;
    }
    return 4;
  }

  /** @inheritDoc */
  getInitiativeOptions() {
    return {
      check: false,
    };
  }

  _prepareTemplates() {
    const templates = this.system.templates ?? {};
    const key = "templates";
    const labels = pf1.config.hauntTemplateLabels;
    const template = {
      base: templates,
      custom: new Set(),
      standard: new Set(),
      get total() {
        return this.standard.union(this.custom);
      },
      get names() {
        return [...this.standard.map((t) => labels[t] || t), ...this.custom];
      },
    };

    this.system[key] = template;

    if (Array.isArray(template.base)) {
      for (const c of template.base) {
        if (labels[c]) template.standard.add(c);
        else template.custom.add(c);
      }
    }
  }

  /**
   * Get challenge rating.
   *
   * @returns {number} - CR
   */
  getCR() {
    return this.system.details?.cr?.total ?? 0;
  }
}
