export {};

declare module "./document-like-model.mjs" {
  interface DocumentLikeModel {
    /**
     * Pseudo-document ID
     *
     * Only used for data storage.
     */
    _id: string;
    /**
     * Name
     */
    name?: string;
  }
}
