_id: ZfCUTi1MiLgR4T7I
_key: '!items!ZfCUTi1MiLgR4T7I'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/items/inventory/flute.jpg
name: Bardic Performance
system:
  actions:
    - _id: ohyjhnb4bta2gy9g
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        units: round
      name: Use
      range:
        units: seeText
  associations:
    classes:
      - Bard
  description:
    value: >-
      <p>A bard is trained to use the <em>Perform</em> skill to create magical
      effects on those around him, including himself if desired. He can use this
      ability for a number of rounds per day equal to 4 + his <em>Charisma</em>
      modifier. At each level after 1st a bard can use bardic performance for 2
      additional rounds per day. Each round, the bard can produce any one of the
      types of bardic performance that he has mastered, as indicated by his
      level.</p>

      <p>Starting a bardic performance is a <em>standard action</em>, but it can
      be maintained each round as a <em>free action</em>. Changing a bardic
      performance from one effect to another requires the bard to stop the
      previous performance and start a new one as a <em>standard action</em>. A
      bardic performance cannot be disrupted, but it ends immediately if the
      bard is killed, <em>paralyzed</em>, <em>stunned</em>, knocked
      <em>unconscious</em>, or otherwise prevented from taking a <em>free
      action</em> to maintain it each round. A bard cannot have more than one
      bardic performance in effect at one time.</p>

      <p>At 7th level, a bard can start a bardic performance as a <em>move
      action</em> instead of a <em>standard action</em>. At 13th level, a bard
      can start a bardic performance as a <em>swift action</em>.</p>

      <p>Each bardic performance has audible <em>components</em>, visual
      <em>components</em>, or both.</p>

      <p>If a bardic performance has audible components, the targets must be
      able to hear the bard for the performance to have any effect, and many
      such performances are language dependent (as noted in the description). A
      <em>deaf</em> bard has a 20% chance to fail when attempting to use a
      bardic performance with an audible component. If he fails this check, the
      attempt still counts against his daily limit. Deaf creatures are immune to
      bardic performances with audible components.</p>

      <p>If a bardic performance has a visual component, the targets must have
      line of sight to the bard for the performance to have any effect. A
      <em>blind</em> bard has a 50% chance to fail when attempting to use a
      bardic performance with a visual component. If he fails this check, the
      attempt still counts against his daily limit. Blind creatures are immune
      to bardic performances with visual components.</p>
  sources:
    - id: PZO1110
      pages: '35'
  subType: classFeat
  tag: classFeat_bardicPerformance
  uses:
    maxFormula: 4 + @abilities.cha.mod + ((@class.level * 2) - 2)
    per: day
type: feat
