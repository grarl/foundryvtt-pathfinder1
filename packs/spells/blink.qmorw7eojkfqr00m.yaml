_id: qmorw7eojkfqr00m
_key: '!items!qmorw7eojkfqr00m'
_stats:
  coreVersion: '12.331'
folder: EmIafjKl7hvbinLh
img: icons/magic/defensive/illusion-evasion-echo-purple.webp
name: Blink
system:
  actions:
    - _id: 5dzk688k673shgv0
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        dismiss: true
        units: round
        value: '@cl'
      name: Use
      range:
        units: personal
      target:
        value: you
  components:
    somatic: true
    verbal: true
  description:
    value: >-
      <p>You "blink" quickly back and forth between the Material Plane and the
      Ethereal Plane and look as though you're winking in and out of reality at
      random. <i>Blink</i> has several effects, as follows.</p><p>Physical
      attacks against you have a 50% miss chance, and the Blind-Fight feat
      doesn't help opponents, since you're ethereal and not merely invisible. If
      the attack is capable of striking ethereal creatures, the miss chance is
      only 20% (for concealment).</p><p>If the attacker can see invisible
      creatures, the miss chance is also only 20%. (For an attacker who can both
      see and strike ethereal creatures, there is no miss chance.) Likewise,
      your own attacks have a 20% miss chance, since you sometimes go ethereal
      just as you are about to strike.</p><p>Any individually targeted spell has
      a 50% chance to fail against you while you're blinking unless your
      attacker can target invisible, ethereal creatures. Your own spells have a
      20% chance to activate just as you go ethereal, in which case they
      typically do not affect the Material Plane (but they might affect targets
      on the Ethereal Plane).</p><p>While blinking, you take only half damage
      from area attacks (but full damage from those that extend onto the
      Ethereal Plane).</p><p>Although you are only partially visible, you are
      not considered invisible and targets retain their Dexterity bonus to AC
      against your attacks. You do receive a +2 bonus on attack rolls made
      against enemies that cannot see invisible creatures.</p><p>You take only
      half damage from falling, since you fall only while you are
      material.</p><p>While blinking, you can step through (but not see through)
      solid objects. For each 5 feet of solid material you walk through, there
      is a 50% chance that you become material. If this occurs, you are shunted
      off to the nearest open space and take 1d6 points of damage per 5 feet so
      traveled.</p><p>Since you spend about half your time on the Ethereal
      Plane, you can see and even attack ethereal creatures. You interact with
      ethereal creatures roughly the same way you interact with material
      ones.</p><p>An ethereal creature is invisible, incorporeal, and capable of
      moving in any direction, even up or down. As an incorporeal creature, you
      can move through solid objects, including living creatures.</p><p>An
      ethereal creature can see and hear the Material Plane, but everything
      looks gray and insubstantial. Sight and hearing on the Material Plane are
      limited to 60 feet.</p><p>Force effects and abjurations affect you
      normally. Their effects extend onto the Ethereal Plane from the Material
      Plane, but not vice versa. An ethereal creature can't attack material
      creatures, and spells you cast while ethereal affect only other ethereal
      things.</p><p>Certain material creatures or objects have attacks or
      effects that work on the Ethereal Plane. Treat other ethereal creatures
      and objects as material.</p>
  learnedAt:
    bloodline:
      Starsoul: 3
    class:
      arcanist: 3
      bard: 3
      magus: 3
      psychic: 3
      skald: 3
      sorcerer: 3
      wizard: 3
  level: 3
  school: trs
  sources:
    - id: PZO1110
      pages: '250'
  sr: false
type: spell
