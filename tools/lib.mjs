import fsp from "node:fs/promises";
import fs from "node:fs";
import path from "node:path"; // Can't use posix paths since watcher doesn't heed that

/**
 * Debounce Function
 *
 * @param {Function} fn
 * @param {number} delay
 * @returns {Function}
 */
export const debounce = (fn, delay) => {
  let timeout;
  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => fn(...args), delay);
  };
};

/**
 * Create folder. Ignore errors from it already existing.
 *
 * @param {string} path - Path to create
 * @returns {Promise<undefined>}
 * @throws
 */
export async function mkdir(path) {
  return fsp.mkdir(path).catch((err) => {
    if (err.code !== "EEXIST") throw err;
  });
}

/**
 * @param {string} path
 * @returns {object|undefined} - JSON object or undefined on read error.
 */
export function readJSONSync(path) {
  let data;
  try {
    data = fs.readFileSync(path, { encoding: "utf-8" });
  } catch {
    return;
  }
  return JSON.parse(data);
}

/**
 * @param {string} path
 * @returns {object|undefined} - JSON object or undefined on read error.
 */
export async function readJSON(path) {
  let data;
  try {
    data = await fsp.readFile(path, { encoding: "utf-8" });
  } catch {
    return;
  }
  return JSON.parse(data);
}

/**
 * readdir() but always returns posix paths
 *
 * @param {string} path
 * @param {object} [options]
 * @param {boolean} [options.recursive]
 */
export async function readdir(path, { recursive = false } = {}) {
  try {
    const results = await fsp.readdir(path, { recursive });
    if (process.platform === "win32") {
      return results.map((p) => p.replaceAll("\\", "/"));
    }
    return results;
  } catch (err) {
    //console.error(err); // Happens when the folder doesn't exist, we don't care
    return [];
  }
}

/**
 * Filesystem Watcher
 *
 * @example
 * ```js
 * const ac = new AbortController();
 * const watcher = new Watcher("help", /\.md$/i, { signal: ac.signal });
 * watcher.on("change", function someFileHandler(filename) { ... });
 * watcher.watch();
 * ```
 */
export class Watcher {
  static DEBOUNCE = 100;

  /** @type {string} */
  #path;
  /** @type {RegExp} */
  #match;
  /** @type {AsyncIterator} */
  #watcher;

  /** @type {number|undefined} */
  #delay;

  constructor(basePath, match, { signal, delay } = {}) {
    this.#match = match;
    this.#path = basePath;

    this.#delay = delay;

    this.#watcher = fsp.watch(basePath, { recursive: true, signal });
  }

  /**
   * Event handlers
   *
   * @type {Record<string, Array<Function>>}
   */
  #events = {};

  /**
   * Debounce timers
   *
   * @type {Record<string, Map<string, number>>}
   */
  #timers = {};

  /**
   * Register event handler
   *
   * @param {string} event
   * @param {Function} fn
   */
  on(event, fn) {
    this.#timers[event] ??= new Map();

    this.#events[event] ??= [];
    this.#events[event].push(fn);
  }

  /**
   * Event initial handler and debouncer
   *
   * Without the debouncing, some events happen 2-4 times (on Windows)
   *
   * @param {string} event - Filesystem event
   * @param {string} filename - Filename
   */
  onEvent(event, filename) {
    if (!filename) return;

    // Shunt rename events to change.
    // There's no special information on the original name, so nothing different can be done.
    // Rename can trigger twice, once for original file and again for the new name,
    // but either of these events might not be in our tracked location.
    if (event === "rename") event = "change";

    const timers = this.#timers[event];
    if (!timers) return;

    const fns = this.#events[event] ?? [];
    if (fns.length === 0) return;

    const fullpath = path.join(this.#path, filename).split(path.sep).join(path.posix.sep);
    if (!this.#match.test(fullpath)) return;

    let timer = timers.get(fullpath);
    if (timer !== undefined) clearTimeout(timer);

    timer = setTimeout(() => this.doCallbacks(event, fullpath), this.#delay ?? Watcher.DEBOUNCE);
    timers.set(fullpath, timer);
  }

  /**
   * Event callback handler.
   *
   * @param event
   * @param filename
   * @param isRename
   */
  async doCallbacks(event, filename) {
    this.#timers[event].delete(filename);

    // Test if the file exists, rename events could cause old name to be reported
    try {
      await fsp.access(filename);
    } catch {
      return; // File doesn't exist
      // TODO: Delete the now non-existing file in dist or wherever?
    }

    const fns = this.#events[event] ?? [];
    if (fns.length === 0) return;

    for (const fn of fns) {
      fn(filename);
    }
  }

  /**
   * Enter watch loop
   */
  async watch() {
    try {
      for await (const event of this.#watcher) {
        const { eventType, filename } = event;
        this.onEvent(eventType, filename);
      }
    } catch (err) {
      if (err.name === "AbortError") {
        this.close();
        return;
      }
      throw err;
    }
  }

  close() {
    Object.values(this.#timers).forEach((n) => clearTimeout(n));
  }
}
