/**
 * Copy static files to reduce need for full rebuild.
 */

import path from "node:path/posix";
import fsp from "node:fs/promises";
import { ViteLoggerPF } from "./vite-logger.mjs";
import { Watcher } from "./lib.mjs";

/** @type {Watcher} */
let watcher;

const ac = new AbortController();

/**
 * A plugin that watches the `publicDir` for changes to system.json and other static files.
 *
 * @returns {import("vite").Plugin}
 */
export default function syncStaticFiles() {
  return {
    name: "sync-static-files",

    configResolved(config) {
      // Don't do anything for full builds
      if (config.command !== "serve") return;

      const logger = new ViteLoggerPF(config.logger);

      watcher = new Watcher("public", /(system|template)\.json$/i, { signal: ac.signal });

      /**
       * Handle an individual file changes and copy them if changed
       *
       * @param {string} file - The file that changed
       * @returns {Promise<void>}
       */
      const fileHandler = async (file) => {
        if (!file.endsWith("json")) return;

        // Transform OS path into Foundry-suitable path
        const filepathUrl = file.replace(/public\//, "").replace(/^\/+|\/+$/g, "");

        // Copy to `dist` to persist the change
        const distFile = path.join(config.build.outDir, filepathUrl);
        await fsp.copyFile(file, distFile);
        logger.info(`Copied ${file} to ${distFile}`);
      };

      watcher.on("change", fileHandler);
      watcher.watch();
    },

    async buildEnd() {
      ac.abort();
    },
  };
}
